import datetime

result = {
  "data":
    [{'id': 0, 'date': '2022-02-23', 'count': 0, 'total': 819, 'timespan': 1645585177894,
      'sign': '4857a8aa73fd3746baee93c21575d04d'},
     {'id': 0, 'date': '2022-02-24', 'count': 0, 'total': 800, 'timespan': 1645585177894,
      'sign': '5e23e86f5d7ee9ca860f908aeab626c0'},
     {'id': 0, 'date': '2022-02-25', 'count': 0, 'total': 800, 'timespan': 1645585177894,
      'sign': 'bfd35177d6e194938fc81425a2ab8b46'},
     {'id': 0, 'date': '2022-02-26', 'count': 0, 'total': 802, 'timespan': 1645585177895,
      'sign': '9cde736855cfe37a9cd00e16e315b73e'},
     {'id': 0, 'date': '2022-02-27', 'count': 0, 'total': 800, 'timespan': 1645585177896,
      'sign': 'ac381d8d2a19753c300e8a2c8a71fe72'},
     {'id': 0, 'date': '2022-02-28', 'count': 0, 'total': 700, 'timespan': 1645585177897,
      'sign': '300b4c351d93d24872d583a27832e709'},
     {'id': 0, 'date': '2022-03-01', 'count': 1, 'total': 700, 'timespan': 1645585177897,
      'sign': '1f2e907fcfb33f687726636d65a57776'}]

}

for data in result['data']:
  data_date = datetime.datetime.strptime(data['date'], '%Y-%m-%d')
  two_days_laster = datetime.datetime.now().date() + + datetime.timedelta(days=2)
  if data['count'] > 0 and data_date.date() >= two_days_laster:
    temp_url = f"https://hk.sz.gov.cn:8118/passInfo/confirmOrder?checkinDate={data['date']}&t={data['timespan']}&s={data['sign']}"
    print(temp_url)

