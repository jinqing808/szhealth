
var selectCheckInDate = "\u9078\u64C7\u5165\u4F4F\u6642\u9593";
var selectDateTitle = "\u8ACB\u9078\u64C7\u65E5\u671F";
var infoMsg = "\u8ACB\u5148\u5B8C\u5584\u500B\u4EBA\u4FE1\u606F\u624D\u80FD\u9810\u8A02\u9152\u5E97";
var selectDate = [];
var submitLoading = "\u63D0\u4EA4\u4E2D";
var submitRemport = "\u7D2F\u8A08\u723D\u7D043\u6B21\u5F8C\u8CEC\u6236\u5C07\u7121\u6CD5\u518D\u9810\u8A02\u9152\u5E97\uFF0C\u78BA\u5B9A\u8981\u9810\u8A02\u55CE\uFF1F";
var confirmResver = "\u78BA\u5B9A\u8981\u9810\u7D04\u55CE";
var resverrSuccess = "\u9810\u7D04\u6210\u529F";
var reveserCount = "\u53EF\u9810\u7D04\u6578";
var revser = "\u9810\u7D04";

var total = "\u7E3D";
var can = "\u53EF";
var no = "\u7121";
var detailDay = "\u65E5";

$(function () {
    //initCalendar();
    getDistrictList();
});

function initCalendar() {
    var currentDate = new Date();
    var currentDateStr = currentDate.format("yyyy-MM");
    $("#yearMonth").html(currentDateStr);
    var html = '';
    for (var i = 0; i < 7; i++) {
        var now = new Date();
        now.setDate(currentDate.getDate() + i);
        var day = now.getDate();
        var dateStr = now.format("yyyy-MM-dd");
        var cla = "";
        if (i == 0) {
            cla = "active";
            $("#hidCheckinDate").val(dateStr);
        }
        html += '<a href="javascript:;" onclick="selectDate1(this)" class="' + cla + '" data-value="' + dateStr + '">' + paddingZero(day) + '</a>';
    }
    $("#seletime-date").html(html);
}

function paddingZero(value) {
    return value < 10 ? "0" + value : value.toString();
}

function selectDate1(obj) {
    $(".seletime-date a").removeClass("active");
    $(obj).addClass("active");
    var dateStr = $(obj).data("value");
    $("#hidCheckinDate").val(dateStr);
    var dateArr = dateStr.split('-');
    var currentDateStr = dateArr[0] + "-" + dateArr[1];
    $("#yearMonth").html(currentDateStr);
    getDistrictList();

}

//获取区域列表数据
function getDistrictList() {
    $.ajax({
        type: "post",
        url: rootPath + "districtHousenumLog/getList",
        data: {
           // checkinDate: $("#hidCheckinDate").val()
        },
        beforeSend: function () {
            layerHelp.loading();
        },
        success: function (data) {
            layer.closeAll();
            if (parseInt(data.status) == 200) {
                var szHtml = '';
               // var otherHtml = '';
                $(data.data).each(function (i, item) {
                    var dateArr=item.date.split('-');
                    var year='';
                    var day='';
                    if(dateArr.length>2){
                       year=dateArr[0]+"-"+dateArr[1];
                       day=dateArr[2];
                    }
                    szHtml += _.template($("#tempList").html())({
                        data: item,
                        year:year,
                        day:day
                    });
                    // if (item.areaType == 1) {
                    //     szHtml += html;
                    // } else if (item.areaType == 3) {
                    //     otherHtml += html;
                    // }
                });
                if (szHtml == '') {
                    szHtml = '<div style="text-align: center;margin-top: 10px;">暂无数据</div>';
                }
                // if (otherHtml == '') {
                //     otherHtml = '<div style="text-align: center;margin-top: 10px;">暂无数据</div>';
                // }
                $("#divSzArea").html(szHtml);
               // $("#otherCityArea").html(otherHtml);

            } else {
                layerHelp.msg(data.msg);
            }
        },
        error: function (e) {
            layer.closeAll();
        },
        complete: function () {

        }
    });
}

function showComfirm(obj) {
    var checkinDate=$(obj).attr("data-date");
    var timespan=$(obj).attr("data-timespan");
    var sign=$(obj).attr("data-sign");
    $("#hidCheckinDate").val(checkinDate);
    $("#hidTimespan").val(timespan);
    $("#hidSign").val(sign);
    $("#mSueccss,#winSueccss").show();
}

function closeComfirm() {
    $("#mSueccss,#winSueccss").hide();
}

function redirectOrder() {
    var checkinDate=  $("#hidCheckinDate").val();
    var timespan=$("#hidTimespan").val();
    var sign= $("#hidSign").val();
    closeComfirm();
    window.location.href = "/passInfo/confirmOrder?checkinDate=" + checkinDate+"&t="+timespan+"&s="+sign;
}


