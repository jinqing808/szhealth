from bs4 import BeautifulSoup

with open(f"detail_7_success.html", 'r') as f:
  content = f.read()
  # print(content)
  soup = BeautifulSoup(content, 'html.parser')

  sections = soup.find_all('section', {"class":'card_info'})
  for section in sections:
    children = section.findChildren("span", {"class": "orange"})
    for child in children:
      children2 = child.findChildren("em")
      for child2 in children2:
        print(child2.text)
        buttons = section.findChildren("button", {"class": "orange"})
        for button in buttons:
          print(button['data-date'])

