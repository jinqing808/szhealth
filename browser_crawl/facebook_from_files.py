# Import Module
import os
import re
import logging
import json

from datetime import datetime

# Read text File
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def read_text_file(file_path):
  with open(file_path, 'r') as f:
    html = f.read()
    # 先解析 當前的 parking id
    ids = re.findall(
        r"href=\"https://www.facebook.com/marketplace/item/(\d+)/", html)

    logger.debug(f"ids count: {len(ids)}")


# iterate through all file
if __name__ == '__main__':
  read_text_file("Facebook_Marketplace.html")
