# -*- coding: utf-8 -*-
import os
import sys
import re
import pymysql
import time
import logging
import datetime
logger = logging.getLogger(__name__)
# Add the root folder path to the sys.path list
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
logger.info(f"BASE_DIR: {BASE_DIR}")
sys.path.append(BASE_DIR)

import tutorial.settings as settings

dbparams = dict(
    host=settings.MYSQL_HOST,  # 读取settings中的配置
    port=settings.MYSQL_PORT,
    db=settings.MYSQL_DBNAME,
    user=settings.MYSQL_USER,
    passwd=settings.MYSQL_PASSWD,
    charset='utf8mb4',  # 编码要加上，否则可能出现中文乱码问题
    cursorclass=pymysql.cursors.DictCursor,
    use_unicode=False,
)


def read_source_ids():
  connection = pymysql.connect(**dbparams)
  resp = {}
  try:
    with connection.cursor() as cursor:
      # Read a single record
      sql = "SELECT `source_id`, `source_create_time` FROM `car_fb_car`"
      cursor.execute(sql)
      results = cursor.fetchall()
      for result in results:
        resp[result['source_id'].decode()] = result['source_create_time']
  except Exception as e:
    raise
  finally:
    connection.close()
    return resp


class FB_DBHelper():
  '''这个类也是读取settings中的配置，自行修改代码进行操作'''

  def __init__(self):
    # **表示将字典扩展为关键字参数,相当于host=xxx,db=yyy....
    self.conn = pymysql.connect(**dbparams)

  def connect(self):
    return self.conn

  def check_exists(self, source_id):
    try:
      # Read a single record
      cursor = self.conn.cursor()

      sql = "select 1 from car_fb_car where source_id = %s"
      params = (source_id)
      cursor.execute(sql, params)
      result_one = cursor.fetchone()
      if result_one:
        return True
      else:
        return False

    except Exception as e:
      self.conn.rollback()
      raise

  # 创建数据库
  def insert(self, item):
    logger.info('Enter Insert')
    item['create_time'] = time.strftime('%Y-%m-%d %H:%M:%S',
                                        time.localtime(time.time()))
    item['update_time'] = time.strftime('%Y-%m-%d %H:%M:%S',
                                        time.localtime(time.time()))
    item['car_model'] = re.sub('\r?\n', ' ', item['car_model'])
    item['car_model'] = item['car_model'][:128]
    item['short_review'] = re.sub('\r?\n', ' ', item['short_review'])
    item['short_review'] = item['short_review'][:500]

    try:
      # Read a single record
      cursor = self.conn.cursor()

      sql = "insert into car_fb_car (car_brand, car_model, year_of_manufactory, short_review, price, source_id, source_create_time, create_time, update_time, url) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
      params = (item['car_brand'], item['car_model'],
                item['year_of_manufactory'], item['short_review'], item['price'],
                item['source_id'], item['source_create_time'], item['create_time'],
                item['update_time'], item['url'])
      cursor.execute(sql, params)
      self.conn.commit()

      return item
    except Exception as e:
      self.conn.rollback()
      raise


  # 错误处理方法


  def __del__(self):
    if self.conn:
      self.conn.close()


if __name__ == '__main__':
  db_helper = FB_DBHelper()
  db_helper.insert({
      'car_brand': None,
      'car_model': '123',
      'source_id': '14446',
      'year_of_manufactory': None,
      'short_review': 'r3242',
      'price': '3244',
      'source_create_time': '2021-09-01',
      'create_time': '2021-09-01',
      'update_time': '2021-09-01',
      'url': 'https',
  })