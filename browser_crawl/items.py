# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class FBItem(scrapy.Item):
  # define the fields for your item here like:
  source_id = scrapy.Field()  # 编号
  car_brand = scrapy.Field()  # 宝马什么的品牌名?
  car_model = scrapy.Field()
  year_of_manufactory = scrapy.Field()  # 出厂年份
  short_review = scrapy.Field()  # 简评
  price = scrapy.Field()  # 价格
  source_create_time = scrapy.Field()  # 最后更新时间
  create_time = scrapy.Field()  # 自己数据库的创建和更新时间
  update_time = scrapy.Field()
  url = scrapy.Field()
  phone = scrapy.Field()
