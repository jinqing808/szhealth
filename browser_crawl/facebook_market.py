import time
import re
import logging
import traceback
import os
import locale
import sys
from bs4 import BeautifulSoup
from lxml import etree
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from multiprocessing.dummy import Pool as ThreadPool
from datetime import datetime, timedelta
import queue

# Add the root folder path to the sys.path list
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(f"BASE_DIR: {BASE_DIR}")
sys.path.append(BASE_DIR)

from browser_crawl.fb_dbhelper import FB_DBHelper
from browser_crawl.items import FBItem
from browser_crawl.fb_collection import collect

os.environ["PYTHONIOENCODING"] = "utf-8"
locale.setlocale(category=locale.LC_ALL, locale="zh_HK.UTF-8")

LOG_DIR = os.path.join(BASE_DIR, "log")
logging.basicConfig(filename=os.path.join(LOG_DIR, "facebook.{}.log".format(datetime.now().strftime("%Y-%m-%d %H:%M"))),
                    filemode='w',
                    format='%(asctime)s,%(msecs)d %(name)s [%(levelname)s] %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def my_excepthook(excType, excValue, traceback, logger=logger):
  # catch the system exception
  logger.error("Logging an uncaught exception",
               exc_info=(excType, excValue, traceback))


sys.excepthook = my_excepthook

CON_CURRENT_NUM = 10
driver_queue = queue.Queue(CON_CURRENT_NUM)
retry_queue = queue.Queue(CON_CURRENT_NUM * 3)
pool = ThreadPool(CON_CURRENT_NUM)


def parse_item_page(item_id):
  url = f"https://www.facebook.com/marketplace/item/{item_id}"
  try:
    driver = driver_queue.get()
    logger.info(f"[Thread] url to parse: {url}")
    item_retry_time = 1
    item_max_retry_time = 4
    while(True):
      # 最多重試 4 次
      try:
        driver.get(url)
        html = driver.page_source
        return parse_item_html(html, item_id, url)
      except Exception as e:
        error_msg = str(e)
        logger.error(f"Item id({item_id}) error:{error_msg}， 當前重試次數為：{item_retry_time}")
        if item_retry_time <= item_max_retry_time and ('Invalid publish_time_str' in error_msg or 'page crash' in error_msg or 'session delete' in error_msg):
          item_retry_time = item_retry_time + 1
        else:
          raise

  except Exception as e:
    traceback.print_exc()
    error_msg = str(e)
    logger.exception(f"[Thread] Item {item_id}, failed: {error_msg}")
    collect.critical("{} ,{},{},{},{}".format(url, item_id, 'error', '', error_msg))
    pass
  finally:
    # make sure the driver can be quit
    logger.info("[Thread] 瀏覽器 放回到 Queue")
    driver_queue.put(driver)


def parse_item_html(html, item_id, url):
  logger.info(f"[Thread] Item id: {item_id}")
  publish_time_str = re.findall(
      r"class=\"sjgh65i0\"><div>.*dir=\"auto\">(.*?)前<", html)

  logger.info(publish_time_str)
  if not publish_time_str:
    # with open(os.path.join(LOG_DIR, f"{item_id}.html"), "w") as fp:
    #   fp.write(html)
    raise ValueError(f"Invalid publish_time_str:{publish_time_str}")

  publish_time_str = publish_time_str[0].replace(
      '已在<!-- -->', '').replace('大約', '').replace(' ', '')
  logger.info(
      f"[Thread] 時間String：{publish_time_str}")

  db_healper = FB_DBHelper()
  if db_healper.check_exists(item_id):
    collect.critical("{} ,{},{},{},{}".format(url, item_id, 'ignore', '已存在', ''))
    return publish_time_str

  source_create_time = datetime.now()
  if '分鐘' in publish_time_str:
    mins_before = int(publish_time_str.strip('分鐘').strip())
    source_create_time = source_create_time - timedelta(minutes=mins_before)
  elif '小時' in publish_time_str:
    hours_before = int(publish_time_str.strip('小時').strip())
    source_create_time = source_create_time - timedelta(hours=hours_before)
  elif '天' in publish_time_str:
    days_before = int(publish_time_str.strip('天').strip())
    source_create_time = source_create_time - timedelta(days=days_before)

  logger.info(f"[Thread] source_create_time:{source_create_time}")

  soup = BeautifulSoup(html, "html.parser")
  dom = etree.HTML(str(soup))
  car_model = dom.xpath(
      "//div[@class='k4urcfbm nkil8b7s']//div/span[contains(@class, 'hrzyx87i')]")[0].text

  price = dom.xpath("//div[@class='aov4n071']/div/span")[0].text
  price = price.replace('HK', '').replace('$', '').replace(',', '').strip()
  logger.info(f"[Thread]car model: {car_model}, price: {price}")

  short_review = dom.xpath("//div[@class='n851cfcs']/div[@class='aahdfvyu']/div/div/div/span")
  if short_review:
    short_review = short_review[0].text
  else:
    short_review = ''
  # logger.info(f"[Thread] short_review:{short_review}")

  item = FBItem()
  item['source_id'] = item_id
  item['car_brand'] = None
  item['year_of_manufactory'] = None
  item['car_model'] = car_model
  item['price'] = price
  item['url'] = url
  item['short_review'] = short_review
  item['source_create_time'] = source_create_time.strftime('%Y-%m-%d %H:%M:%S')

  db_healper.insert(item)

  collect.critical("{} ,{},{},{},{}".format(url, item_id, 'insert', '', ''))

  return publish_time_str


if __name__ == '__main__':
  try:
    # Web scrapper for infinite scrolling page
    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')  # 規避google bug
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("window-size=1920,1080")
    chrome_options.add_argument("disable-infobars")
    chrome_options.add_argument("--lang=zh-tw")
    chrome_options.add_experimental_option('prefs', {'intl.accept_languages': 'zh-tw'})

    driver = webdriver.Chrome(options=chrome_options)

    thread_driver = []
    for _ in range(CON_CURRENT_NUM):
      driver2 = webdriver.Chrome(options=chrome_options)
      thread_driver.append(driver2)
      driver_queue.put(driver2)

    driver.get("https://www.facebook.com/marketplace/category/vehicles?minPrice=10000&sortBy=creation_time_descend&exact=false")
    time.sleep(2)  # Allow 2 seconds for the web page to open
    # You can set your own pause time. My laptop is a bit slow so I use 1 sec
    scroll_pause_time = 1
    screen_height = driver.execute_script(
        "return window.screen.height;")   # get the screen height of the web
    i = 1

    retry_time = 0
    max_retry_time = 60

    days_to_retrieve = 2  # 必須要設置1天或以上（或者應該從配置文件獲取）。當設置2天的時候，如果前一天有問題，可以第二天再run的時候可以把數據都爬回來
    item_ids = []
    while retry_time <= max_retry_time:
      # scroll one screen height each time
      driver.execute_script(
          "window.scrollTo(0, {screen_height}*{i});".format(screen_height=screen_height, i=i))
      i += 1
      time.sleep(scroll_pause_time)
      # update scroll height each time after scrolled, as the scroll height can change after we scrolled the page
      # 先不用 save screen ， 如果要debug 再開起來
      # driver.save_screenshot(os.path.join(LOG_DIR, 'screenshot_{}.png'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))))
      scroll_height = driver.execute_script(
          "return document.body.scrollHeight;")
      logger.info(
          f"i({i}), screen_height*i({screen_height * i}), scroll_height({scroll_height})")
      # Break the loop when the height we need to scroll to is larger than the total scroll height
      if (screen_height) * i > scroll_height:
        logger.info("screen_height > scroll_height")
        i -= 1
        retry_time += 1
      else:
        retry_time = 0

        html = driver.page_source

        current_item_ids = re.findall(
            r"href=\"/marketplace/item/(\d+)/", html)

        new_item_ids = [
            item for item in current_item_ids if item not in item_ids]

        if len(new_item_ids) > 0:
          logger.info(f"新車數據： {len(new_item_ids)}")
          results = pool.map(parse_item_page, new_item_ids)

          # 檢測最後一個是不是到 要求的天數，如果到了，就不再運行
          publish_time_str = results[-1]
          logger.info(
              f"時間：{publish_time_str}, 總車數據：{len(current_item_ids)}")
          if publish_time_str == f"{days_to_retrieve}天":
            for _ in range(CON_CURRENT_NUM):
              # CON_CURRENT_NUM 個瀏覽器都可以獲取，說明可以關閉所有Thread裡面的瀏覽器了。
              driver_queue.get()
            break

        item_ids = current_item_ids

    if retry_time > max_retry_time:
      raise ValueError('重試超過最大次數，不能正常完成！')
  except Exception as e:
    logger.exception(e)
    pass
  finally:
    # make sure the driver can be quit
    logger.info("關閉所有 selenium 瀏覽器")
    driver.quit()
    for d in thread_driver:
      d.quit()
