# -*- coding: utf-8 -*-
import os
import datetime
from tutorial.settings import BASE_DIR

f = os.path.join(BASE_DIR, "log", "fb_{}_collect.csv".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
with open(f, mode="w", encoding="utf-8") as fp:
  fp.write("{}, {} ,{},{},{},{}\n".format("time", "url", "source_id", "action", "reason", "remark"))


class FBLogColl:

  def __init__(self):
    self.f = f
    self.fp = open(self.f, mode="a", encoding="utf-8")

  def critical(self, text):
    """
      text 的格式 是：  "url", "source_id", "action", "reason", "remark"
    """
    s = "{},{}".format(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), text)
    self.fp.write(s)
    self.fp.write("\n")
    self.fp.flush()

  def __del__(self):
    if self.fp:
      self.fp.close()


collect = FBLogColl()
