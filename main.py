import time
import re
import logging
import traceback
import os
import locale
import sys
from bs4 import BeautifulSoup
from lxml import etree
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
from multiprocessing.dummy import Pool as ThreadPool
from datetime import datetime, timedelta
import queue

# Add the root folder path to the sys.path list
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(f"BASE_DIR: {BASE_DIR}")
sys.path.append(BASE_DIR)

os.environ["PYTHONIOENCODING"] = "utf-8"
locale.setlocale(category=locale.LC_ALL, locale="zh_HK.UTF-8")

LOG_DIR = os.path.join(BASE_DIR, "SZHealth", "log")
logging.basicConfig(filename=os.path.join(LOG_DIR, "szhealth.{}.log".format(datetime.now().strftime("%Y-%m-%d %H:%M"))),
                    filemode='w',
                    format='%(asctime)s,%(msecs)d %(name)s [%(levelname)s] %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

# def my_excepthook(exc_type, exc_value, my_traceback, my_logger=logger):
#   # catch the system exception
#   my_logger.error("Logging an uncaught exception",
#                   exc_info=(exc_type, exc_value, my_traceback))
#
#
# sys.excepthook = my_excepthook

CON_CURRENT_NUM = 10
# driver_queue = queue.Queue(CON_CURRENT_NUM)
# retry_queue = queue.Queue(CON_CURRENT_NUM * 3)
# pool = ThreadPool(CON_CURRENT_NUM)

if __name__ == '__main__':
  # try:
  chrome_options = Options()
  chrome_options.add_argument('--no-sandbox')
  # chrome_options.add_argument('--headless')  # 規避google bug
  chrome_options.add_argument('--disable-gpu')
  chrome_options.add_argument("--disable-dev-shm-usage")
  chrome_options.add_argument("window-size=393,851")
  chrome_options.add_argument("w3c=true")
  chrome_options.add_argument("disable-infobars")
  chrome_options.add_argument("--lang=zh-tw")
  chrome_options.add_experimental_option('prefs', {'intl.accept_languages': 'zh-tw'})

  # get the login session cookie first
  driver = webdriver.Chrome(executable_path="/Users/John/Downloads/chromedriver", options=chrome_options)

  driver.get("https://hk.sz.gov.cn:8118/userPage/login")

  # Select(driver.find_element(By.ID, "select_certificate")).select_by_index(2)
  select = Select(driver.find_element(value='select_certificate'))
  #
  # # select by visible text
  select.select_by_visible_text('往來港澳通行證')

  element = driver.find_element(value="input_idCardNo")
  element.send_keys("C15783160")

  element = driver.find_element(value="input_pwd")
  element.send_keys("jinqing808")

# except Exception as e:
#   logger.exception(e)
#   pass
# finally:
#   # make sure the driver can be quit
#   logger.info("關閉所有 selenium 瀏覽器")
#   driver.quit()
