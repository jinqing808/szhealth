import json
import traceback

import requests
import threading
import time
import datetime
from bs4 import BeautifulSoup

# Configuration
session_id = 'f70f024e-343d-465c-8950-164cf4853c20'
# num_of_thread = 50
num_of_thread_each_time = 10
num_of_thread_confirm_each_time = 20
start_hour = 10
start_minute = 0
end_hour = 10
end_minute = 2

headers = {
  'Cookie': f"isolate-web-session-id={session_id}",
  'User-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Mobile Safari/537.36'
}

class Confirm_Worker(threading.Thread):

  def __init__(self, url, num):
    threading.Thread.__init__(self)
    self.url = url
    self.num = num

  def run(self):
    with open(f"confirm_thread_{self.num}.log", 'w') as log_file:
      try:
        r = requests.get(self.url, headers=headers, allow_redirects=False)
        log_file.write(str(r.status_code) + '\n')
        log_file.write(str(r.headers) + '\n')
        # print(r)

        if r.status_code in (301, 302):
          r = requests.get('https://hk.sz.gov.cn:8118' + r.headers['Location'], headers=headers, allow_redirects=False)
          log_file.write(str(r.status_code) + '\n')
          log_file.write(str(r.headers) + '\n')

          if r.status_code in (301, 302):
            r = requests.get('https://hk.sz.gov.cn:8118' + r.headers['Location'], headers=headers, allow_redirects=False)
            log_file.write(str(r.status_code) + '\n')
            log_file.write(str(r.headers) + '\n')

        with open(f"confirm_{self.num}.html", 'w') as f:
          f.write(r.text)
      except:
        log_file.write(traceback.format_exc())


class Worker(threading.Thread):
  already_printed = False

  def __init__(self, lock, num):
    threading.Thread.__init__(self)
    self.lock = lock
    self.num = num

  def run(self):
    try:
      # print('thread start')
      r = requests.get('https://hk.sz.gov.cn:8118/passInfo/detail', headers=headers)

      content = r.text
      # print(content)

      self.lock.acquire()
      try:
        if not type(self).already_printed:
          if r.status_code == 200:

            if "總預約數" in content:
              # 只有當 返回的 html裡面有 總預約數才把 already_printed 設置為true
              type(self).already_printed = True
              with open(f"detail_{self.num}_success.html", 'w') as f:
                f.write(content)

              soup = BeautifulSoup(content, 'html.parser')

              confirm_thread_array = []
              confirm_num = 0

              sections = soup.find_all('section', {"class": 'card_info'})
              have_quote = False

              for section in sections:
                children = section.findChildren("span", {"class": "orange"})
                if len(children) > 0:
                  have_quote = True
                  for child in children:
                    children2 = child.findChildren("em")
                    for child2 in children2:
                      print(child2.text)
                      buttons = section.findChildren("button", {"class": "orange"})
                      for button in buttons:
                        url = f"https://hk.sz.gov.cn:8118/passInfo/confirmOrder?checkinDate={button['data-date']}&t={button['data-timespan']}&s={button['data-sign']}"
                        print(url)
                        # for i in range(num_of_thread_confirm_each_time):
                        #   my_confirm_worker = Confirm_Worker(url, confirm_num)
                        #   my_confirm_worker.start()
                        #   confirm_thread_array.append(my_confirm_worker)
                        #   confirm_num += 1

              if not have_quote:
                print('沒有期!')
            else:
              with open(f"detail_{self.num}_error.html", 'w') as f:
                f.write(content)

      finally:
        self.lock.release()

    except:
      # if just network error， mute it
      pass




if __name__ == '__main__':
  lock = threading.Lock()

  thread_array = []

  start_time = datetime.time(start_hour, start_minute, 0)
  while datetime.datetime.now().time() < start_time:
    time.sleep(0.1)

  end_time = datetime.time(end_hour, end_minute, 0)
  num = 0
  while datetime.datetime.now().time() < end_time and not Worker.already_printed:
    for i in range(num_of_thread_each_time):
      my_worker = Worker(lock, num)
      my_worker.start()
      thread_array.append(my_worker)
      num += 1
    #   每10秒 發一輪請求，用來 match 服務器上的時間，看什麼時間會中
    time.sleep(10)

  for t in thread_array:
    t.join()

  print("Done.")
